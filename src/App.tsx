import { useState } from "react";
import "./App.scss";
import { CONTENT } from "./types/consts/button-content.const";

function App(): JSX.Element {
  const [contentIndex, setContentIndex] = useState(0);

  return (
    <div className="App">
      <h1>React Buttons</h1>
      <div className="buttons">
        <div className="button-container">
          <button
            className={`button ${contentIndex === 0 ? "active" : ""}`}
            onClick={() => setContentIndex(0)}
          >
            First
          </button>
        </div>
        <div className="button-container">
          <button
            className={`button ${contentIndex === 1 ? "active" : ""}`}
            onClick={() => setContentIndex(1)}
          >
            Second
          </button>
        </div>
        <div className="button-container">
          <button
            className={`button ${contentIndex === 2 ? "active" : ""}`}
            onClick={() => setContentIndex(2)}
          >
            Third
          </button>
        </div>
        <div className="button-container">
          <button
            className={`button ${contentIndex === 3 ? "active" : ""}`}
            onClick={() => setContentIndex(3)}
          >
            Fourth
          </button>
        </div>
      </div>
      <div className="list-container">
        <ul>
          {CONTENT[contentIndex].map((item) => (
            <li key={item}> {item} </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default App;
