# Run project locally

Clone Project from Gitlab

## `git clone git@gitlab.com:r1106/rcg-buttons-react.git`

# Installation

Install missing packages in the root folder

## `npm install`

# Start project in development mode

In the project directory, you can run:

## `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes. You may also see any lint errors in the console.

# Pages Overview

![First Button Screen](src/images/readme/react_buttons_01.png "First Button Screen")

![Second Button Screen](src/images/readme/react_buttons_02.png "Second Button Screen")

![Third Button Screen](src/images/readme/react_buttons_03.png "Third Button Screen")

![Fourth Button Screen](src/images/readme/react_buttons_04.png "Fourth Button Screen")

